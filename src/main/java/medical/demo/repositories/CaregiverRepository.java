package medical.demo.repositories;

import medical.demo.entities.Caregiver;
import medical.demo.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CaregiverRepository extends CrudRepository<Caregiver, Integer> {

    @Query(value = "SELECT u " +
            "FROM Caregiver u " +
            "ORDER BY u.name")
    List<Caregiver> getAllOrdered();

    @Query(value = "SELECT c " +
            "FROM Caregiver c " +
            "INNER JOIN FETCH c.patients i"
    )
    List<Caregiver> getAllFetch();
}