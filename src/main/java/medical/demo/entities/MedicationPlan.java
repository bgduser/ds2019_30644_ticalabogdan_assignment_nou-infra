package medical.demo.entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

import static javax.persistence.GenerationType.AUTO;

@Entity
@Table(name = "medication_plan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = AUTO)
    private Integer id;

    @Column(name = "daily_interval")
    private String dailyInterval;

    @Column(name = "period_treatment")
    private String periodOfTreatment;

    @ManyToMany(mappedBy = "medicationPlans")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Column(name = "medications")
    @JsonIgnoreProperties("medicationPlans")
    private Set<Medication> medications;

    @OneToOne(mappedBy = "medicationPlan")
    @JsonIgnoreProperties("medicationPlan")
    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public MedicationPlan() {

    }

    public MedicationPlan(String dailyInterval, String periodOfTreatment, Set<Medication> medications, Patient patient) {
        this.dailyInterval = dailyInterval;
        this.periodOfTreatment = periodOfTreatment;
        this.medications = medications;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDailyInterval() {
        return dailyInterval;
    }

    public void setDailyInterval(String dailyInterval) {
        this.dailyInterval = dailyInterval;
    }

    public String getPeriodOfTreatment() {
        return periodOfTreatment;
    }

    public void setPeriodOfTreatment(String periodOfTreatment) {
        this.periodOfTreatment = periodOfTreatment;
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlan that = (MedicationPlan) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(dailyInterval, that.dailyInterval) &&
                Objects.equals(periodOfTreatment, that.periodOfTreatment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dailyInterval, periodOfTreatment);
    }
}
