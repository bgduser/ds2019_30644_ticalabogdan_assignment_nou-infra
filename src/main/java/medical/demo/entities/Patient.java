package medical.demo.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

import static javax.persistence.GenerationType.AUTO;
import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
public class Patient {
    @Id
    @GeneratedValue(strategy = AUTO)
    @Column(name = "id_patient")
    private Integer id;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name = "birthDate")
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date birthDate;

    @Column(name = "gender")
    private String gender;

    @Column(name = "address")
    private String address;

    @Column(name = "medicalRecord", length = 100)
    private String medicalRecord;

    @Column(name = "email", length = 200)
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_doctor",  nullable = true)
    @JsonIgnoreProperties("patients")
    //@JsonBackReference
    private Doctor doctor;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "id_caregiver",  nullable = true)
    @JsonIgnoreProperties("patients")
    private Caregiver caregiver;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "medication_plan_id", referencedColumnName = "id")
    @JsonIgnoreProperties("patient")
    private MedicationPlan medicationPlan;

    public Patient() {

    }

    public Patient(String name, Date birthDate, String gender, String address, String medicalRecord, String email, String password, Caregiver caregiver, Doctor doctor,MedicationPlan medicationPlan) {
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.email = email;
        this.password = password;
        this.caregiver = caregiver;
        this.doctor = doctor;
        this.medicationPlan=medicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Caregiver getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(Caregiver caregiver) {
        this.caregiver = caregiver;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(id, patient.id) &&
                Objects.equals(name, patient.name) &&
                Objects.equals(birthDate, patient.birthDate) &&
                Objects.equals(gender, patient.gender) &&
                Objects.equals(address, patient.address) &&
                Objects.equals(medicalRecord, patient.medicalRecord) &&
                Objects.equals(email, patient.email) &&
                Objects.equals(password, patient.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, birthDate, gender, address, medicalRecord, email, password);
    }
}
