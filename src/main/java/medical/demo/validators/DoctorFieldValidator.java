package medical.demo.validators;

import medical.demo.entities.Doctor;
import medical.demo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class DoctorFieldValidator {
    private static final Log LOGGER = LogFactory.getLog(DoctorFieldValidator.class);
    private static final EmailValidator EMAIL_VALIDATOR = new EmailValidator();

    public static void validateInsertOrUpdate(Doctor doctor) {

        List<String> errors = new ArrayList<>();
        if (doctor == null) {
            errors.add("doctorDto is null");
            throw new IncorrectParameterException(Doctor.class.getSimpleName(), errors);
        }

        if (doctor.getEmail() == null || !EMAIL_VALIDATOR.validate(doctor.getEmail())) {
            errors.add("Doctor email has invalid format");
        }
        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(DoctorFieldValidator.class.getSimpleName(), errors);
        }
    }

}
