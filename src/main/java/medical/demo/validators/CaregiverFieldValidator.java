package medical.demo.validators;

import medical.demo.entities.Caregiver;
import medical.demo.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class CaregiverFieldValidator {
    private static final Log LOGGER = LogFactory.getLog(CaregiverFieldValidator.class);
    private static final EmailValidator EMAIL_VALIDATOR = new EmailValidator();

    public static void validateInsertOrUpdate(Caregiver caregiver) {

        List<String> errors = new ArrayList<>();
        if (caregiver == null) {
            errors.add("caregiverDto is null");
            throw new IncorrectParameterException(Caregiver.class.getSimpleName(), errors);
        }

        if (caregiver.getEmail() == null || !EMAIL_VALIDATOR.validate(caregiver.getEmail())) {
            errors.add("Caregiver email has invalid format");
        }
        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(CaregiverFieldValidator.class.getSimpleName(), errors);
        }
    }

}
