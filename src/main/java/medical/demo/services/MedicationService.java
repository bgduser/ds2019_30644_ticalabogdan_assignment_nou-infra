package medical.demo.services;
;
import medical.demo.entities.Medication;
import medical.demo.errorhandler.ResourceNotFoundException;
import medical.demo.repositories.MedicationRepository;
import medical.demo.validators.MedicationFieldValidator;
import medical.demo.validators.MedicationFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public Medication findById(Integer id) {
        Optional<Medication> medication = medicationRepository.findById(id);
        if (!medication.isPresent()) {
            throw new ResourceNotFoundException("Medication", "id_medication", id);
        }
        return medication.get();
    }

    public List<Medication> findAll() {
        return(medicationRepository.getAllOrdered());
    }

    public Medication insert(Medication medication) {

        MedicationFieldValidator.validateInsertOrUpdate(medication);

        return medicationRepository.save(medication);

    }

    public Integer update(Medication medication) {

        Optional<Medication> medicationS = medicationRepository.findById(medication.getId());

        if (!medicationS.isPresent()) {
            throw new ResourceNotFoundException("Medication", "medication id", medication.getId().toString());
        }

        MedicationFieldValidator.validateInsertOrUpdate(medication);

        return medicationRepository.save(medication).getId();
    }

    
    public void delete(Medication medication) {
        this.medicationRepository.deleteById(medication.getId());
    }

}


