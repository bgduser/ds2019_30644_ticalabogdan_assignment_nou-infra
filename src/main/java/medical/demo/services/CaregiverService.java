package medical.demo.services;

import medical.demo.entities.Caregiver;
import medical.demo.errorhandler.ResourceNotFoundException;
import medical.demo.repositories.CaregiverRepository;
import medical.demo.validators.CaregiverFieldValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CaregiverService {

    @Autowired
    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }


    public Caregiver findById(Integer id) {
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);

        if (!caregiver.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "id_caregiver", id);
        }
        return caregiver.get();
    }

    public List<Caregiver> findAll() {

        return caregiverRepository.getAllOrdered();


    }

    public Caregiver insert(Caregiver caregiver) {

        CaregiverFieldValidator.validateInsertOrUpdate(caregiver);

        return caregiverRepository.save(caregiver);

    }


    public Integer update(Caregiver caregiver) {

        Optional<Caregiver> caregiverS = caregiverRepository.findById(caregiver.getId());

        if (!caregiverS.isPresent()) {
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiver.getId().toString());
        }

        CaregiverFieldValidator.validateInsertOrUpdate(caregiver);

        return caregiverRepository.save(caregiver).getId();
    }

    public void delete(Caregiver caregiver) {

        this.caregiverRepository.deleteById(caregiver.getId());
    }

}
