package medical.demo.controller;

import medical.demo.entities.Caregiver;
import medical.demo.entities.Doctor;
import medical.demo.entities.MedicationPlan;
import medical.demo.entities.Patient;
import medical.demo.repositories.CaregiverRepository;
import medical.demo.repositories.DoctorRepository;
import medical.demo.repositories.MedicationPlanRepository;
import medical.demo.services.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller

@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    DoctorRepository doctorRepository;

    @Autowired
    CaregiverRepository caregiverRepository;

    @Autowired
    MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping("/signup")
    public String showSignUpForm(Patient patient) {
        return "add-patient";
    }

    @RequestMapping("/home")
    public String showPatientHome(Model model) {

        model.addAttribute("listOfPatients", patientService.findAll());
        return "index";
    }

    @PostMapping("/addPatientX")
    public String addPatientX(@Valid Patient patient, BindingResult result, Model model) {
        patientService.insert(patient);
        model.addAttribute("listOfPatients", patientService.findAll());

        return "index";
    }

    @GetMapping("/")
    public List<Patient> findAll() {

        return patientService.findAll();
    }


    @PostMapping("/addPatientt/{idDoctor}/{idCaregiver}")
    public ResponseEntity<Void> addPatientt(@PathVariable("idDoctor") int idDoctor, @PathVariable("idCaregiver") int idCaregiver, @RequestBody Patient patient) {
        Optional<Doctor> doctor = doctorRepository.findById(idDoctor);
        Optional<Caregiver> caregiver = caregiverRepository.findById(idCaregiver);
        if (doctor == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (caregiver == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        patient.setDoctor(doctor.get());
        patient.setCaregiver(caregiver.get());
        doctor.get().getPatients().add(patient);
        caregiver.get().getPatients().add(patient);

        patientService.insert(patient);



        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/addPatient")
    //in loc de valid sa am request body
    public String addPatient(@Valid Patient patient, Model model) {
        Optional<Doctor> doctor = doctorRepository.findById(patient.getDoctor().getId());
        Optional<Caregiver> caregiver = caregiverRepository.findById(patient.getCaregiver().getId());
        //Optional<MedicationPlan> medicationPlan = medicationPlanRepository.findById(patient.getMedicationPlan().getId());

//        if (!doctor.isPresent()) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        if (!caregiver.isPresent()) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        if (!medicationPlan.isPresent()) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }

        patient.setDoctor(doctor.get());
        doctor.get().getPatients().add(patient);

        patient.setCaregiver(caregiver.get());
        caregiver.get().getPatients().add(patient);

//        patient.setMedicationPlan(medicationPlan.get());
//        medicationPlan.get().setPatient(patient);

        patientService.insert(patient);
        model.addAttribute("listOfPatients", patientService.findAll());
//        return new ResponseEntity<>(HttpStatus.OK);
        return "index";
    }

    @PostMapping("/edit")
    ResponseEntity<Void> updatePatient(@RequestBody Patient patient) {

        if (patientService.findById(patient.getId()) == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        patientService.update(patient);

        return new ResponseEntity<>(HttpStatus.OK);
    }


    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        Patient patient = patientService.findById(id);
        model.addAttribute("patient", patient);
        return "update-patient";
    }

    @PostMapping("/update/{id}")
    public String updatePatient(@PathVariable("id") int id, @Valid Patient patient, BindingResult result, Model model) {
        if (result.hasErrors()) {
            patient.setId(id);
            return "update-patient";
        }

        patientService.insert(patient);
        model.addAttribute("listOfPatients", patientService.findAll());
        return "index";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String deletePatient(@PathVariable("id") int id, Model model) {
        Patient patient = patientService.findById(id);
        patientService.delete(patient);
        model.addAttribute("listOfPatients", patientService.findAll());
        return "index";
    }


}


