package medical.demo.controller;

import medical.demo.entities.Caregiver;
import medical.demo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping("/signup")
    public String showSignUpForm(Caregiver caregiver) {
        return "add-caregiver";
    }

    @RequestMapping("/home")
    public String showCaregiverHome(Model model) {

        model.addAttribute("listOfCaregivers", caregiverService.findAll());
        return "index";
    }

    @PostMapping("/addCaregiver")
    public String addCaregiver(@Valid Caregiver caregiver, BindingResult result, Model model) {

        caregiverService.insert(caregiver);
        model.addAttribute("listOfCaregivers", caregiverService.findAll());

        return "index";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        Caregiver caregiver = caregiverService.findById(id);
        model.addAttribute("caregiver", caregiver);
        return "update-caregiver";
    }

    @PostMapping("/update/{id}")
    public String updateCaregiver(@PathVariable("id") int id, @Valid Caregiver caregiver, BindingResult result, Model model) {
        if (result.hasErrors()) {
            caregiver.setId(id);
            return "update-caregiver";
        }

        caregiverService.insert(caregiver);
        model.addAttribute("listOfCaregivers", caregiverService.findAll());
        return "index";
    }

    //@DeleteMapping("/delete/{id}")
    @RequestMapping(value = "/delete/{id}",method =RequestMethod.GET)
    public String deleteCaregiver(@PathVariable("id") int id, Model model) {
        Caregiver caregiver = caregiverService.findById(id);
        caregiverService.delete(caregiver);
        model.addAttribute("listOfCaregivers", caregiverService.findAll());
        return "index";
    }
}
