package medical.demo.controller;

import medical.demo.entities.Doctor;
import medical.demo.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping("/signup")
    public String showSignUpForm(Doctor doctor) {
        return "add-doctor";
    }

    @RequestMapping("/home")
    public String showDoctorHome(Model model) {

        model.addAttribute("listOfDoctors", doctorService.findAll());
        return "index";
    }

    @PostMapping("/addDoctor")
    public String addDoctor(@Valid Doctor doctor, BindingResult result, Model model) {
        doctorService.insert(doctor);
        model.addAttribute("listOfDoctors", doctorService.findAll());

        return "index";
    }

    @GetMapping("/edit/{id}")
    public String showUpdateForm(@PathVariable("id") int id, Model model) {
        Doctor doctor = doctorService.findById(id);
        model.addAttribute("doctor", doctor);
        return "update-doctor";
    }

    @PostMapping("/update/{id}")
    public String updateDoctor(@PathVariable("id") int id, @Valid Doctor doctor, BindingResult result, Model model) {
        if (result.hasErrors()) {
            doctor.setId(id);
            return "update-doctor";
        }

        doctorService.insert(doctor);
        model.addAttribute("listOfDoctors", doctorService.findAll());
        return "index";
    }

    //@DeleteMapping("/delete/{id}")
    @RequestMapping(value = "/delete/{id}",method =RequestMethod.GET)
    public String deleteDoctor(@PathVariable("id") int id, Model model) {
        Doctor doctor = doctorService.findById(id);
        doctorService.delete(doctor);
        model.addAttribute("listOfDoctors", doctorService.findAll());
        return "index";
    }
}


