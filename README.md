Clone the project to your computer
Import the project in InteliJ (or your IDE)
Create a database schema "medical" in MySQL
Change the line 26 from application.properties file located in /src/main/resources to allow table creation in the DB: spring.jpa.hibernate.ddl-auto = create
Run the application
Check is the tables were created
Change the line 26 from application.properties file located in /src/main/resources to allow table creation in the DB: spring.jpa.hibernate.ddl-auto = validate